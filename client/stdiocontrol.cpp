#include "stdiocontrol.h"
#include <QSocketNotifier>
#include <QtDebug>
#include <iostream>
#include <fcntl.h>
#include <unistd.h>

StdioControl::StdioControl(QObject* parent)
  : QObject(parent)
{
  setvbuf(stdin, nullptr, _IONBF, 0);
  fcntl(fileno(stdin), F_SETFL, O_NONBLOCK);
  notifier = new QSocketNotifier(fileno(stdin), QSocketNotifier::Read, this);
  QObject::connect(notifier, SIGNAL(activated(int)), this, SLOT(stdinReadyRead()));
}

void StdioControl::stdinReadyRead()
{
  notifier->setEnabled(false);
  std::string newData;
  char tempBuffer[256];
  while (true) {
    ssize_t bytes = read(0, tempBuffer, sizeof(tempBuffer));
    if (bytes > 0) {
      buffer += QByteArray(tempBuffer, bytes);
    }
    if (bytes < (ssize_t)sizeof(tempBuffer)) {
      break;
    }
  }
  notifier->setEnabled(true);

  while (true) {
    int linePos = buffer.indexOf('\n');
    if (linePos < 0) {
      break;
    }
    QByteArray line = buffer.left(linePos);
    buffer = buffer.mid(linePos + 1);
    QList<QByteArray> args = line.split('\t');

    if (args[0] == "updateStatus" && args.length() >= 2) {
      emit updateStatus(args[1].toInt());
    } else if (args[0] == "showMessage" && args.length() >= 5) {
      emit showMessage(QString::fromUtf8(args[1]), QString::fromUtf8(args[2]), QSystemTrayIcon::MessageIcon(args[3].toInt()), args[4].toInt());
    } else if (args[0] == "needsAuth" && args.length() >= 3) {
      emit getAuth(QString::fromUtf8(args[1]), QString::fromUtf8(args[2]));
    } else if (args[0] == "needsAuthWithChallenge" && args.length() >= 4) {
      emit getAuthWithChallenge(QString::fromUtf8(args[1]), QString::fromUtf8(args[2]), args[3] == "1", QString::fromUtf8(args[4]));
    } else if (args[0] == "needsWebAuth" && args.length() >= 2) {
      emit getWebAuth(QString::fromUtf8(args[1]));
    }
  }
  if (feof(stdin)) {
    emit hangup();
    return;
  }
}

void StdioControl::authenticate(const QString& scope, const QString& username, const QString& password)
{
  std::cout << "authenticate1\t"
    << qPrintable(scope) << "\t"
    << qPrintable(username) << "\t"
    << qPrintable(password) << std::endl;
}

void StdioControl::authenticate(const QString& scope, const QString& username, const QString& password, const QString& challenge)
{
  std::cout << "authenticate2\t"
    << qPrintable(scope) << "\t"
    << qPrintable(username) << "\t"
    << qPrintable(password) << "\t"
    << qPrintable(challenge) << std::endl;
}

void StdioControl::authenticate(const QString& scope, const QString& username, const QByteArray& password, const QString& challenge)
{
  std::cout << "authenticate3\t"
    << qPrintable(scope) << "\t"
    << qPrintable(username) << "\t"
    << password.constData() << "\t"
    << qPrintable(challenge) << std::endl;
}

void StdioControl::connectToVpn()
{
  std::cout << "connectToVpn" << std::endl;
}

void StdioControl::disconnectFromVpn()
{
  std::cout << "disconnectFromVpn" << std::endl;
}

void StdioControl::shutdown()
{
  std::cout << "shutdown" << std::endl;
}
