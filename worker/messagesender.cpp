#include "messagesender.h"
#include "messagereceiver.h"
#include <iostream>

void MessageSender::addReceiver(const std::string& key, MessageReceiver* receiver) {
  rcv[key] = receiver;
  receiver->sender = this;
}

void MessageSender::removeReceiver(MessageReceiver* receiver) {
  std::string key;
  for (auto pair : rcv) {
    if (pair.second == receiver) {
      key = pair.first;
      break;
    }
  }
  if (!key.empty()) {
    // Don't remove the entry because there could be an iterator on the stack
    rcv[key] = nullptr;
  }
}

bool MessageSender::send(const std::string& key, const std::string& message) {
  if (rcv.count(key)) {
    auto target = rcv[key];
    if (target) {
      target->dispatch(message);
      return true;
    }
  }
  return false;
}
