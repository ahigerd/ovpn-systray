#ifndef OSC_STDIOCONTROL_H
#define OSC_STDIOCONTROL_H

#include <QObject>
#include <QByteArray>
#include <QSystemTrayIcon>
class QSocketNotifier;

class StdioControl : public QObject {
Q_OBJECT
public:
  StdioControl(QObject* parent = nullptr);

signals:
  void updateStatus(int status);
  void showMessage(const QString& title, const QString& message, QSystemTrayIcon::MessageIcon icon, int timeout);
  void getAuth(const QString& scope, const QString& message);
  void getAuthWithChallenge(const QString& scope, const QString& message, bool echo, const QString& challenge);
  void getWebAuth(const QString& url);
  void hangup();

public slots:
  void authenticate(const QString& scope, const QString& username, const QString& password);
  void authenticate(const QString& scope, const QString& username, const QString& password, const QString& challenge);
  void authenticate(const QString& scope, const QString& username, const QByteArray& password, const QString& challenge);
  void connectToVpn();
  void disconnectFromVpn();
  void shutdown();

private slots:
  void stdinReadyRead();

private:
  QSocketNotifier* notifier;
  QByteArray buffer;
};

#endif
