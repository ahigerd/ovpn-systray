#include "linebuffer.h"
#include "eventloop.h"
#include <unistd.h>

LineBuffer::LineBuffer() : EventSource() {
  // initializers only
}

LineBuffer::~LineBuffer() {
  close();
}

void LineBuffer::setFd(int fd) {
  if (eventFd >= 0) {
    EventLoop::instance()->remove(this);
  }
  eventFd = fd;
  if (eventFd >= 0) {
    EventLoop::instance()->add(this);
  }
}

bool LineBuffer::fillBuffer() {
  if (eventFd < 0) {
    return false;
  }

  ssize_t size;
  char buf[256];
  do {
    size = ::read(eventFd, buf, 256);
    if (size > 0) {
      buf[size] = '\0';
      buffer += buf;
    }
  } while (size > 0);
  return buffer.length() > 0;
}

bool LineBuffer::canReadLine() const {
  return buffer.find('\n') != std::string::npos;
}

std::string LineBuffer::readLine() {
  size_t pos = buffer.find('\n');
  if (pos != std::string::npos) {
    std::string line = buffer.substr(0, pos + 1);
    buffer.erase(0, pos + 1);
    return line;
  }
  return std::string();
}

void LineBuffer::triggered() {
  fillBuffer();
  std::string line;
  while (!((line = readLine())).empty()) {
    while (line.back() == '\r' || line.back() == '\n' || line.back() == ' ') {
      line.pop_back();
    }
    processLine(line);
  }
}

void LineBuffer::close() {
  if (eventFd >= 0) {
    ::close(eventFd);
    setFd(-1);
  }
}

void LineBuffer::processLine(const std::string&) {
  // Default implementation does nothing
}
