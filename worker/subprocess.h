#ifndef OSW_SUBPROCESS_H
#define OSW_SUBPROCESS_H

#include "linebuffer.h"
#include <initializer_list>
#include <string>
#include <vector>

class Subprocess : public LineBuffer {
public:
  static bool reapChildren();

  Subprocess(const std::vector<std::string>& argv);
  Subprocess(std::initializer_list<std::string> argv);
  virtual ~Subprocess();

  bool isRunning() const;
  bool kill();
  void write(const std::string& data);
  int waitForFinished();
  int exitStatus() const;

protected:
  int pid;
  int status;

private:
  void run(const std::string& cmdline, const std::vector<const char*> c_argv);
  void stopTracking();
};

#endif
