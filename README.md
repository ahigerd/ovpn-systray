ovpn-systray
============

`ovpn-systray` is a simple frontend for [OpenVPN](https://openvpn.net)
on Linux. It is loosely inspired by [Tunnelblick](tunnelblick.net) for
macOS, providing a VPN status icon with a quick connection menu.

Features
--------

* Easily connect and disconnect from VPNs
* Monitor network status in the system tray or notification area
* Automatically reconnect if connection is interrupted
* Supports static challenges such as two-factor authentication

Limitations
-----------

* Does not currently support more than one VPN profile
* Does not currently support dynamic challenges
* Is not intended to provide OpenVPN configuration tools
* Is not currently fully compatible with systemd-resolved

Requirements
------------

`ovpn-systray` depends on the following packages:

* QtCore 5.x
* QtGui 5.x
* QtWidgets 5.x
* iproute2
* OpenVPN

Additionally, if you are using GNOME Shell 3.26 or later, you will
need a systray / appindicator extension for compatibility. The
`gnome-shell-extension-appindicator` package is available for Ubuntu
and related distros, or you can [install it from the GNOME Extensions
site](https://extensions.gnome.org/extension/615/appindicator-support/).

Building and Installation
-------------------------

To build ovpn-systray:

```
make
```

To prepare ovpn-systray to be run from the build directory:

```
sudo make local
```

To install ovpn-systray into `/usr/bin`:

```
sudo make install
```

License
-------

ovpn-systray is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License and/or the GNU Lesser General Public
License for more details.

The icons used by ovpn-systray are copied or adapted from
[Adwaita](https://github.com/GNOME/adwaita-icon-theme) by the [GNOME
project](http://www.gnome.org/). These icons are licensed under the
Creative Commons Attribution-Share Alike 3.0 United States License
or the GNU Lessser General Public License version 3.

You should have received a copy of the GNU General Public License
version 2 and the GNU Lesser General Public License version 3
along with this software. If not, see <https://www.gnu.org/licenses/>.

To view a copy of the CC-BY-SA 3.0 license, visit
http://creativecommons.org/licenses/by-sa/3.0/ or send a letter to
Creative Commons, 171 Second Street, Suite 300, San Francisco,
California 94105, USA.
