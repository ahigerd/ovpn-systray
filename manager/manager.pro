TEMPLATE = app
DESTDIR = ..
OBJECTS_DIR = .tmp
MOC_DIR = .tmp
RCC_DIR = .tmp
TARGET = ovpn-systray
INCLUDEPATH += .
QT = core
DEFINES += QT_DEPRECATED_WARNINGS

HEADERS += iconprocess.h   managementsocket.h   ../common/signalhandler.h
SOURCES += iconprocess.cpp managementsocket.cpp ../common/signalhandler.cpp

SOURCES += main.cpp
