#include "managementsocket.h"
#include "eventloop.h"
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <cstring>

ManagementSocket::ManagementSocketListener::ManagementSocketListener(ManagementSocket* parent)
: EventSource(), parent(parent) {
  // initializers only
}

ManagementSocket::ManagementSocketListener::~ManagementSocketListener() {
  if (eventFd >= 0) {
    ::close(eventFd);
  }
}

void ManagementSocket::ManagementSocketListener::setFd(int fd) {
  if (eventFd >= 0) {
    ::close(eventFd);
    EventLoop::instance()->remove(this);
  }
  eventFd = fd;
  if (eventFd >= 0) {
    EventLoop::instance()->add(this);
  }
}

void ManagementSocket::ManagementSocketListener::triggered() {
  int connFd = ::accept(eventFd, nullptr, 0);
  if (connFd < 0) {
    std::cerr << "error acceping connection on socket: " << std::strerror(errno) << std::endl;
    return;
  }

  parent->setFd(connFd);
}

ManagementSocket::ManagementSocket(const std::string& path) : listener(this), path(path) {
  // initializers only
}

ManagementSocket::~ManagementSocket() {
  // If unlinking fails, discard errors
  ::unlink(path.c_str());
}

bool ManagementSocket::listen() {
  int fd = ::socket(AF_UNIX, SOCK_STREAM, 0);
  if (fd <= 0) {
    std::cerr << "error allocating socket: " << std::strerror(errno) << std::endl;
    return false;
  }

  sockaddr_un addr;
  std::memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  std::strncpy(addr.sun_path, path.c_str(), sizeof(addr.sun_path) - 1);

  int err = ::bind(fd, (sockaddr*)&addr, sizeof(addr));
  if (err) {
    std::cerr << "error binding to socket: " << std::strerror(errno) << std::endl;
    return false;
  }

  err = ::listen(fd, 1);
  if (err) {
    std::cerr << "error listening on socket: " << std::strerror(errno) << std::endl;
    return false;
  }

  listener.setFd(fd);

  return true;
}

void ManagementSocket::disconnect() {
  close();
  listener.setFd(-1);
}

void ManagementSocket::processLine(const std::string& line) {
  bool dispatched = false;
  if (line[0] == '>') {
    size_t colon = line.find(':');
    if (colon != std::string::npos) {
      std::string key = line.substr(1, colon - 1);
      if (key == "INFOMSG") {
        dispatched = send("PASSWORD", line);
      } else {
        std::string message = line.substr(colon + 1);
        dispatched = send(key, message);
      }
    }
  }
  if (!dispatched) {
    std::cerr << "[MAN] " << line << std::endl;
  }
}

void ManagementSocket::dispatch(const std::string& message) {
  // This is from stdin, so pass it through as a command
  // TODO: be more judicious to avoid confused deputy issues
  ::write(eventFd, message.c_str(), message.length());
}
