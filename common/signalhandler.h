#ifndef OSC_SIGNALHANDLER_H
#define OSC_SIGNALHANDLER_H

#include <QObject>
class QSocketNotifier;

class SignalHandler : public QObject {
Q_OBJECT
public:
  SignalHandler(QObject* parent = nullptr);

signals:
  void caughtFatalSignal();

private:
  QSocketNotifier* unixNotifier;
};

#endif
