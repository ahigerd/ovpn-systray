#ifndef OSC_PREFSPANEL_H
#define OSC_PREFSPANEL_H

#include <QDialog>
class QLineEdit;
class QCheckBox;
class QComboBox;

class PrefsPanel : public QDialog {
Q_OBJECT
public:
  PrefsPanel(QWidget* parent = nullptr);

private slots:
  void save();
  void browseConfigFile();

private:
  QLineEdit *eConfigPath, *eUsername, *ePassword, *eBinPath, *eSocketPath;
  QCheckBox *cAutoConnect, *cAutoDisconnect, *cAutoWebAuth;
  QComboBox *pResolvConf;
};

#endif
