TEMPLATE = app
DESTDIR = ..
OBJECTS_DIR = .tmp
MOC_DIR = .tmp
RCC_DIR = .tmp
TARGET = ovpn-systray-icon
INCLUDEPATH += .
QT = core gui widgets
DEFINES += QT_DEPRECATED_WARNINGS

HEADERS += trayicon.h   prefspanel.h   authdialog.h   stdiocontrol.h   ../common/signalhandler.h
SOURCES += trayicon.cpp prefspanel.cpp authdialog.cpp stdiocontrol.cpp ../common/signalhandler.cpp

SOURCES += main.cpp
RESOURCES += icons.qrc
