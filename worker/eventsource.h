#ifndef OSW_EVENTSOURCE_H
#define OSW_EVENTSOURCE_H

class EventSource {
public:
  EventSource();
  ~EventSource();

protected:
  friend class EventLoop;
  int eventFd;
  virtual void triggered() = 0;
};

#endif
