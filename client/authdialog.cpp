#include "authdialog.h"
#include <QSettings>
#include <QGridLayout>
#include <QLineEdit>
#include <QCheckBox>
#include <QLabel>
#include <QDialogButtonBox>
#include <QDesktopServices>
#include <QUrl>
#include <QMessageBox>

AuthDialog::AuthDialog(QWidget* parent) : QDialog(parent), webAuthDlg(nullptr) {
  setWindowTitle("ovpn-systray");
  setWindowIcon(QIcon(":/icons/network-vpn-symbolic.symbolic.png"));

  QSettings settings;
  QGridLayout* l = new QGridLayout(this);

  lMessage = new QLabel(this);
  lMessage->setAlignment(Qt::AlignCenter);
  l->addWidget(lMessage, 0, 0, 1, 2);

  QLabel* lUsername = new QLabel("&Username:", this);
  eUsername = new QLineEdit(settings.value("username").toString(), this);
  lUsername->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
  lUsername->setBuddy(eUsername);
  l->addWidget(lUsername, 1, 0);
  l->addWidget(eUsername, 1, 1);

  cUsername = new QCheckBox("&Remember Username", this);
  cUsername->setChecked(!settings.value("username").toString().isEmpty());
  l->addWidget(cUsername, 2, 1);

  QLabel* lPassword = new QLabel("&Password:", this);
  ePassword = new QLineEdit(this);
  ePassword->setEchoMode(QLineEdit::Password);
  lPassword->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
  lPassword->setBuddy(ePassword);
  l->addWidget(lPassword, 3, 0);
  l->addWidget(ePassword, 3, 1);

  cPassword = new QCheckBox("R&emember Password:", this);
  if (!settings.value("password").toString().isEmpty()) {
    ePassword->setText("********");
    cPassword->setChecked(true);
  }
  l->addWidget(cPassword, 4, 1);

  lChallenge = new QLabel("Challenge:", this);
  eChallenge = new QLineEdit(this);
  lChallenge->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
  lChallenge->setBuddy(eChallenge);
  l->addWidget(lChallenge, 5, 0);
  l->addWidget(eChallenge, 5, 1);

  QDialogButtonBox* bb = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, this);
  l->addWidget(bb, 6, 0, 1, 2);

  QObject::connect(bb, SIGNAL(accepted()), this, SLOT(submit()));
  QObject::connect(bb, SIGNAL(rejected()), this, SLOT(reject()));
}

void AuthDialog::getAuth(const QString& scope, const QString& message) {
  lChallenge->setVisible(false);
  eChallenge->setVisible(false);
  showWith(scope, message);
}

void AuthDialog::getAuthWithChallenge(const QString& scope, const QString& message, bool shouldEcho, const QString& challenge) {
  lChallenge->setVisible(true);
  eChallenge->setVisible(true);
  if (challenge.endsWith(":")) {
    lChallenge->setText(challenge);
  } else {
    lChallenge->setText(challenge + ":");
  }
  eChallenge->setEchoMode(shouldEcho ? QLineEdit::Normal : QLineEdit::Password);
  showWith(scope, message);
}

void AuthDialog::getWebAuth(const QString& url) {
  QSettings settings;
  if (settings.value("autoWebAuth").toBool()) {
    QDesktopServices::openUrl(QUrl(url));
    return;
  }

  if (webAuthDlg) {
    webAuthDlg->deleteLater();
  }

  // This is not parented to AuthDialog because it doesn't get positioned correctly
  webAuthDlg = new QMessageBox(
    QMessageBox::Information,
    "ovpn-systray",
    tr("<p>Please authenticate using the following URL:</p><p><a href='%1'>%1</a></p>").arg(url),
    QMessageBox::Ok | QMessageBox::Cancel,
    nullptr
  );
  webAuthDlg->show();
  QObject::connect(this, SIGNAL(destroyed()), webAuthDlg, SLOT(deleteLater()));
  QObject::connect(webAuthDlg, SIGNAL(rejected()), this, SIGNAL(rejected()));
}

void AuthDialog::showWith(const QString& scope, const QString& message) {
  this->scope = scope;
  lMessage->setText(message);
  QWidget* focusTarget = ePassword;
  if (eChallenge->isVisibleTo(this)) {
    eChallenge->clear();
    focusTarget = eChallenge;
  }
  if (!cPassword->isChecked() || ePassword->text().isEmpty()) {
    ePassword->clear();
    focusTarget = ePassword;
  }
  if (!cUsername->isChecked() || eUsername->text().isEmpty()) {
    eUsername->clear();
    focusTarget = eUsername;
  }
  focusTarget->setFocus(Qt::PopupFocusReason);
  show();
}

void AuthDialog::submit() {
  QSettings settings;
  QByteArray savedPassword = settings.value("password").toString().toUtf8();
  QString password = ePassword->text();
  if (password == "********" && !savedPassword.isEmpty()) {
    if (eChallenge->isVisibleTo(this)) {
      emit authenticate(scope, eUsername->text(), savedPassword, eChallenge->text());
    } else {
      emit authenticate(scope, eUsername->text(), QString::fromUtf8(QByteArray::fromBase64(savedPassword)));
    }
  } else {
    if (eChallenge->isVisibleTo(this)) {
      emit authenticate(scope, eUsername->text(), password, eChallenge->text());
    } else {
      emit authenticate(scope, eUsername->text(), password);
    }
  }
  if (cUsername->isChecked()) {
    settings.setValue("username", eUsername->text());
  } else {
    settings.remove("username");
  }
  if (cPassword->isChecked()) {
    if (password != "********") {
      settings.setValue("password", QString::fromUtf8(password.toUtf8().toBase64()));
    }
  } else {
    settings.remove("password");
  }
  accept();
}

void AuthDialog::updateStatus(int state) {
  if (state == 0 || state == 2) {
    // If offline or connected, remove the web auth link
    if (webAuthDlg) {
      webAuthDlg->deleteLater();
      webAuthDlg = nullptr;
    }
  }
}
