#include <QApplication>
#include <QSettings>
#include "../common/signalhandler.h"
#include "stdiocontrol.h"
#include "prefspanel.h"
#include "trayicon.h"
#include "authdialog.h"

int main(int argc, char** argv) {
  QApplication::setApplicationName("ovpn-systray");
  QApplication::setOrganizationName("Alkahest");
  QApplication::setApplicationVersion("0.0.1");
  QApplication app(argc, argv);
  app.setQuitOnLastWindowClosed(false);

  QSettings settings;
  PrefsPanel pp;
  if (settings.value("config").toString().isEmpty()) {
    bool ok = pp.exec() == QDialog::Accepted;
    if (!ok || settings.value("config").toString().isEmpty()) {
      return 1;
    }
  }

  SignalHandler sigHandler;
  QObject::connect(&sigHandler, SIGNAL(caughtFatalSignal()), &app, SLOT(quit()));

  StdioControl ctl;
  TrayIcon icon;
  AuthDialog authDlg;

  QObject::connect(&ctl, SIGNAL(showMessage(QString, QString, QSystemTrayIcon::MessageIcon, int)),
      &icon, SLOT(showMessage(QString, QString, QSystemTrayIcon::MessageIcon, int)));
  QObject::connect(&ctl, SIGNAL(updateStatus(int)), &icon, SLOT(updateStatus(int)));
  QObject::connect(&ctl, SIGNAL(updateStatus(int)), &authDlg, SLOT(updateStatus(int)));
  QObject::connect(&ctl, SIGNAL(getAuth(QString, QString)), &authDlg, SLOT(getAuth(QString, QString)));
  QObject::connect(&ctl, SIGNAL(getAuthWithChallenge(QString, QString, bool, QString)),
      &authDlg, SLOT(getAuthWithChallenge(QString, QString, bool, QString)));
  QObject::connect(&ctl, SIGNAL(getWebAuth(QString)), &authDlg, SLOT(getWebAuth(QString)));

  QObject::connect(&icon, SIGNAL(preferencesClicked()), &pp, SLOT(show()));
  QObject::connect(&icon, SIGNAL(shouldConnect()), &ctl, SLOT(connectToVpn()));
  QObject::connect(&icon, SIGNAL(shouldDisconnect()), &ctl, SLOT(disconnectFromVpn()));
  QObject::connect(&icon, SIGNAL(shutdown()), &ctl, SLOT(shutdown()));
  QObject::connect(&authDlg, SIGNAL(authenticate(QString, QString, QString)),
      &ctl, SLOT(authenticate(QString, QString, QString)));
  QObject::connect(&authDlg, SIGNAL(authenticate(QString, QString, QString, QString)),
      &ctl, SLOT(authenticate(QString, QString, QString, QString)));
  QObject::connect(&authDlg, SIGNAL(authenticate(QString, QString, QByteArray, QString)),
      &ctl, SLOT(authenticate(QString, QString, QByteArray, QString)));
  QObject::connect(&authDlg, SIGNAL(rejected()), &ctl, SLOT(disconnectFromVpn()));

  QObject::connect(&ctl, SIGNAL(hangup()), &app, SLOT(quit()));
  QObject::connect(&icon, SIGNAL(shutdown()), &app, SLOT(quit()), Qt::QueuedConnection);

  return app.exec();
}
