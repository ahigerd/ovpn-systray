#include "eventsource.h"
#include "eventloop.h"

EventSource::EventSource() : eventFd(-1) {
  // initializers only
}

EventSource::~EventSource() {
  if (EventLoop::instance() && eventFd >= 0) {
    EventLoop::instance()->remove(this);
  }
}
