#include "configfile.h"
#include <fstream>

ConfigFile::ConfigFile(const std::string& path)
: resolvConfPath("/etc/resolv.conf"), autoDisconnect(false), useResolvectl(true) {
  std::ifstream f(path, std::ios::in);
  std::string line;

  while (f) {
    std::getline(f, line);
    size_t equals = line.find('=');
    if (equals == std::string::npos) {
      continue;
    }
    std::string key = line.substr(0, equals);
    std::string value = line.substr(equals + 1, line.length() - equals - 1);
    if (value.front() == '"') {
      std::string clean;
      bool escape = false;
      for (char ch : value.substr(1, value.length() - 2)) {
        if (escape) {
          escape = false;
          clean.push_back(ch);
        } else if (ch == '\\') {
          escape = true;
        } else {
          clean.push_back(ch);
        }
      }
      value = clean;
    }
    if (key == "openvpnPath") {
      ovpnPath = value;
    } else if (key == "socketPath") {
      socketPath = value;
    } else if (key == "config") {
      configPath = value;
    } else if (key == "autoDisconnect") {
      autoDisconnect = value == "true";
    } else if (key == "useResolvectl") {
      useResolvectl = value == "true";
    } else if (key == "useResolvConfHead" && value == "true") {
      resolvConfPath = "/etc/resolv.conf.head";
    } else if (key == "useResolvConfTail" && value == "true") {
      resolvConfPath = "/etc/resolv.conf.tail";
    }
  }
}
