#include "passwordhandler.h"
#include <iostream>

PasswordHandler::PasswordHandler() : MessageReceiver() {
  // initializers only
}

void PasswordHandler::dispatch(const std::string& message) {
  // Check for INFOMSG
  if (message.compare(0, 9, ">INFOMSG:") == 0) {
    // send it directly to the GUI
    std::cout << message << std::endl;
    return;
  }

  // this is handled by the GUI, so pass through to stdout unmodified
  std::cout << ">PASSWORD:" << message << std::endl;
}
