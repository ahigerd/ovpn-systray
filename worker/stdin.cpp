#include "stdin.h"
#include <iostream>

Stdin::Stdin() : LineBuffer() {
  setFd(0);
}

void Stdin::processLine(const std::string& line) {
  std::cerr << "[INP] " << line << std::endl;
  send("STDIN", line + "\n");
}
