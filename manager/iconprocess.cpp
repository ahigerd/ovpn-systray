#include "iconprocess.h"
#include <QProcess>
#include <QCoreApplication>
#include <QtDebug>

IconProcess::IconProcess(QObject* parent)
  : QObject(parent), noRespawn(false), process(nullptr), currentStatus(0)
{
  respawnThrottle.setInterval(500);
  respawnThrottle.setSingleShot(true);
  QObject::connect(&respawnThrottle, SIGNAL(timeout()), this, SLOT(launchIcon()));
}

bool IconProcess::launchIcon()
{
  if (process != nullptr) {
    // Do not spawn a process if one is already running
    return false;
  }
  process = new QProcess(this);
  process->setProcessChannelMode(QProcess::ForwardedErrorChannel);
  QObject::connect(process, SIGNAL(readyReadStandardOutput()), this, SLOT(iconReadyRead()));
  QObject::connect(process, SIGNAL(errorOccurred(QProcess::ProcessError)), this, SLOT(iconExited()));
  QObject::connect(process, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(iconExited()));
  process->start(QCoreApplication::instance()->applicationDirPath() + "/ovpn-systray-icon");
  bool ok = process->waitForStarted();
  if (ok) {
    updateStatus(currentStatus);
  }
  return ok;
}

void IconProcess::updateStatus(int status)
{
  currentStatus = status;
  if (process) {
    process->write("updateStatus\t" + QByteArray::number(status) + "\n");
  }
}

void IconProcess::showMessage(const QString& title, const QString& message, int icon, int timeout)
{
  if (process) {
    process->write("showMessage\t" +
        QString(title).replace("\t", " ").toUtf8() + "\t" +
        QString(message).replace("\t", " ").toUtf8() + "\t" +
        QByteArray::number(icon) + "\t" +
        QByteArray::number(timeout) + "\n");
  }
}

void IconProcess::getAuth(const QString& scope, const QString& message)
{
  if (process) {
    process->write("needsAuth\t" +
        QString(scope).replace("\t", " ").toUtf8() + "\t" +
        QString(message).replace("\t", " ").toUtf8() + "\n");
  }
}

void IconProcess::getAuthWithChallenge(const QString& scope, const QString& message, bool echo, const QString& challenge)
{
  if (process) {
    process->write("needsAuthWithChallenge\t" +
        QString(scope).replace("\t", " ").toUtf8() + "\t" +
        QString(message).replace("\t", " ").toUtf8() + "\t" +
        (echo ? "1\t" : "0\t") +
        QString(challenge).replace("\t", " ").toUtf8() + "\n");
  }
}

void IconProcess::getWebAuth(const QString& url)
{
  if (process) {
    process->write(("needsWebAuth\t" + url + "\n").toUtf8());
  }
}

void IconProcess::iconReadyRead()
{
  if (!process) {
    return;
  }
  buffer += process->readAll();

  while (true) {
    int linePos = buffer.indexOf('\n');
    if (linePos < 0) {
      return;
    }
    QByteArray line = buffer.left(linePos);
    buffer = buffer.mid(linePos + 1);
    QList<QByteArray> args = line.split('\t');

    if (args[0] == "connectToVpn") {
      emit connectToVpn();
    } else if (args[0] == "disconnectFromVpn") {
      emit disconnectFromVpn();
    } else if (args[0] == "shutdown") {
      noRespawn = true;
    } else if (args[0] == "authenticate1" && args.length() >= 4) {
      emit authenticate(QString::fromUtf8(args[1]), QString::fromUtf8(args[2]), QString::fromUtf8(args[3]));
    } else if (args[0] == "authenticate2" && args.length() >= 5) {
      emit authenticate(QString::fromUtf8(args[1]), QString::fromUtf8(args[2]), QString::fromUtf8(args[3]), QString::fromUtf8(args[4]));
    } else if (args[0] == "authenticate3" && args.length() >= 5) {
      emit authenticate(QString::fromUtf8(args[1]), QString::fromUtf8(args[2]), args[3], QString::fromUtf8(args[4]));
    }
  }
}

void IconProcess::iconExited()
{
  if (process) {
    process->deleteLater();
    process = nullptr;
  }
  if (noRespawn) {
    emit shutdown();
    return;
  }
  respawnThrottle.start();
}
