#include "ovpnprocess.h"
#include "eventloop.h"
#include "configfile.h"
//#include "messagereceiver.h"
#include <iostream>

OvpnProcess::OvpnProcess(ConfigFile* config)
: Subprocess({
    config->ovpnPath,
    "--config", config->configPath,
    "--log-append", "/dev/stdout",
    "--suppress-timestamps",
    "--management", config->socketPath, "unix",
    "--management-client",
    "--management-query-passwords",
    "--management-up-down",
    "--route-noexec", "--ifconfig-noexec",
    "--setenv", "IV_SSO", "webauth"
}), autoDisconnect(config->autoDisconnect) {
  // initializers only
}

void OvpnProcess::processLine(const std::string& line) {
  bool dispatched = false;
  if (line.find("process restarting") != std::string::npos) {
    dispatched = send("UPDOWN", "CONNECTING");
    if (autoDisconnect) {
      send("CONNECT", "signal SIGTERM\nquit\n");
    }
  }
  if (true || !dispatched) {
    std::cerr << "[LOG] " << line << std::endl;
  }
}

void OvpnProcess::triggered() {
  Subprocess::triggered();
  if (!isRunning()) {
    std::cerr << "OpenVPN process terminated" << std::endl;
    EventLoop::instance()->quit();
  }
}
