#ifndef OSM_ICONPROCESS_H
#define OSM_ICONPROCESS_H

#include <QObject>
#include <QTimer>
class QProcess;

class IconProcess : public QObject {
Q_OBJECT
public:
  IconProcess(QObject* parent = nullptr);

public slots:
  bool launchIcon();
  void updateStatus(int status);
  void showMessage(const QString& title, const QString& message, int icon = 1, int timeout = 10000);
  void getAuth(const QString& scope, const QString& message);
  void getAuthWithChallenge(const QString& scope, const QString& message, bool echo, const QString& challenge);
  void getWebAuth(const QString& url);

signals:
  void authenticate(const QString& scope, const QString& username, const QString& password);
  void authenticate(const QString& scope, const QString& username, const QString& password, const QString& challenge);
  void authenticate(const QString& scope, const QString& username, const QByteArray& password, const QString& challenge);
  void connectToVpn();
  void disconnectFromVpn();
  void shutdown();

private slots:
  void iconReadyRead();
  void iconExited();

private:
  bool noRespawn;
  QProcess* process;
  QByteArray buffer;
  int currentStatus;
  QTimer respawnThrottle;
};

#endif
