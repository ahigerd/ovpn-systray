#ifndef OSW_MANAGEMENTSOCKET_H
#define OSW_MANAGEMENTSOCKET_H

#include "linebuffer.h"
#include "messagesender.h"
#include "messagereceiver.h"

class ManagementSocket : public LineBuffer, public MessageSender, public MessageReceiver {
public:
  ManagementSocket(const std::string& path);
  ~ManagementSocket();

  bool listen();
  void disconnect();

protected:
  void processLine(const std::string& line);
  void dispatch(const std::string& message);

private:
  class ManagementSocketListener : public EventSource {
  public:
    ManagementSocketListener(ManagementSocket* parent);
    ~ManagementSocketListener();

    void setFd(int fd);

  protected:
    virtual void triggered();

  private:
    ManagementSocket* parent;
  } listener;
  friend class ManagementSocketListener;

  std::string path;
};

#endif
