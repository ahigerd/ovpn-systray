#ifndef OSC_AUTHDIALOG_H
#define OSC_AUTHDIALOG_H

#include <QDialog>
#include <QPointer>
class QLineEdit;
class QCheckBox;
class QLabel;
class QMessageBox;

class AuthDialog : public QDialog {
Q_OBJECT
public:
  AuthDialog(QWidget* parent = nullptr);

public slots:
  void getAuth(const QString& scope, const QString& message);
  void getAuthWithChallenge(const QString& scope, const QString& message, bool shouldEcho, const QString& challenge);
  void getWebAuth(const QString& url);
  void updateStatus(int state);

signals:
  void authenticate(const QString& scope, const QString& username, const QString& password);
  void authenticate(const QString& scope, const QString& username, const QString& password, const QString& challenge);
  void authenticate(const QString& scope, const QString& username, const QByteArray& password, const QString& challenge);

private slots:
  void showWith(const QString& scope, const QString& message);
  void submit();

private:
  QLineEdit *eUsername, *ePassword, *eChallenge;
  QCheckBox *cUsername, *cPassword;
  QLabel *lMessage, *lChallenge;
  QString scope;
  QPointer<QMessageBox> webAuthDlg;
};

#endif
