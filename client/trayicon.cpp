#include "trayicon.h"
#include "prefspanel.h"
#include <QTimer>
#include <QApplication>
#include <QScreen>
#include <QAction>
#include <QMenu>
#include <QImage>
#include <QPixmap>
#include <QPalette>
#include <QPainter>
#include <QColor>
#include <QtDebug>

TrayIcon::TrayIcon(QObject* parent)
  : QObject(parent),
    iconOK(":/icons/network-vpn-symbolic.symbolic.png"),
    iconOffline(":/icons/network-vpn-offline-symbolic.symbolic.png"),
    iconConnecting(":/icons/network-vpn-acquiring-symbolic.symbolic.png"),
    iconError(":/icons/network-vpn-no-route-symbolic.symbolic.png"),
    trayIcon(nullptr)
{
  menu = new QMenu();
  aConnect = menu->addAction("&Connect", this, SIGNAL(shouldConnect()));
  aDisconnect = menu->addAction("&Disconnect", this, SIGNAL(shouldDisconnect()));
  menu->addSeparator();
  menu->addAction("&Preferences...", this, SIGNAL(preferencesClicked()));
  menu->addSeparator();
  menu->addAction("E&xit", this, SIGNAL(shutdown()));

  updateStatus(0);

  QTimer* timer = new QTimer(this);
  QObject::connect(timer, SIGNAL(timeout()), this, SLOT(updateVisibility()));
  timer->setTimerType(Qt::VeryCoarseTimer);
  timer->setSingleShot(false);
  timer->start(1000);
  updateVisibility();
}

TrayIcon::~TrayIcon() {
  delete menu;
}

void TrayIcon::showMenu(QSystemTrayIcon::ActivationReason reason) {
  if (reason == QSystemTrayIcon::Trigger) {
    menu->popup(trayIcon->geometry().topRight());
  }
}

void TrayIcon::chooseIcon(const QIcon& icon) {
  // This is a workaround for a Qt5 bug.
  QImage img(24, 24, QImage::Format_RGB32);
  img.fill(menu->palette().button().color());
  QPainter p(&img);
  icon.paint(&p, 0,0, 24, 24);
  currentIcon = QPixmap::fromImage(img);
  if (trayIcon) {
    trayIcon->setIcon(currentIcon);
  }
}

void TrayIcon::updateStatus(int state) {
  switch (state) {
    case 0:
      chooseIcon(iconOffline);
      aConnect->setEnabled(true);
      aDisconnect->setEnabled(false);
      break;
    case 1:
      chooseIcon(iconConnecting);
      aConnect->setEnabled(false);
      aDisconnect->setEnabled(true);
      break;
    case 2:
      chooseIcon(iconOK);
      aConnect->setEnabled(false);
      aDisconnect->setEnabled(true);
      break;
    case 3:
      chooseIcon(iconError);
      aConnect->setEnabled(false);
      aDisconnect->setEnabled(true);
      break;
    default:
      qFatal("unknown status %d received", state);
  }
}

void TrayIcon::updateVisibility() {
  // TODO: While this is reasonably portable, polling is kinda lame.
  // It's possible to hook xcb to get active notifications of this.
  // More research is necessary for other implementations.
  bool available = QSystemTrayIcon::isSystemTrayAvailable();
  if (!available && trayIcon) {
    trayIcon->hide();
    trayIcon->deleteLater();
    trayIcon = nullptr;
    // Unfortunately it appears that QSystemTrayIcon isn't fully up-to-date
    // with the status notifier spec. As a result, when GNOME tears down and
    // recreates its tray, Qt doesn't appropriately update its D-BUS session
    // and it can't create any new icons. As a workaround, the manager can
    // respawn the icon process in its entirety to reinitialize it.
    qApp->quit();
  } else if (available && (!trayIcon || !trayIcon->isVisible())) {
    if (trayIcon) {
      trayIcon->deleteLater();
    }
    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(menu);
    trayIcon->setIcon(currentIcon);
    trayIcon->show();
    QObject::connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(showMenu(QSystemTrayIcon::ActivationReason)));
  }
}

void TrayIcon::showMessage(const QString& title, const QString& message, QSystemTrayIcon::MessageIcon icon, int timeout) {
  if (trayIcon) {
    trayIcon->showMessage(title, message, icon, timeout);
  }
}
