#ifndef OSW_RESOLVCONF_H
#define OSW_RESOLVCONF_H

#include <string>
#include <vector>

class ResolvConf {
public:
  ResolvConf(const std::string& path = "/etc/resolv.conf");

  void save();
  void revert();
  
  void addNameserver(const std::string& server);
  void addSearchDomain(const std::string& domain);

private:
  void load();

  std::string path;
  std::string preamble, postamble;
  std::vector<std::string> searchDomains;
  std::vector<std::string> nameservers;
};

#endif
