#include "signalhandler.h"
#include <QSocketNotifier>
#include <unistd.h>
#include <signal.h>

static int unixFds[2] = { 0, 0 };
static const std::vector<int> signalList{ SIGHUP, SIGTERM, SIGINT, SIGUSR1, SIGUSR2, SIGQUIT, SIGILL, SIGABRT, SIGBUS, SIGSEGV };

static void signalHandler(int signum) {
  char buf[2] = { char(signum & 0xFF), '\0' };
  if (::write(unixFds[1], buf, 1) < 0) {
    qFatal("Fatal error: could not write to pipe");
  }
}

SignalHandler::SignalHandler(QObject* parent)
  : QObject(parent)
{
  if (::pipe(unixFds) != 0) {
    qFatal("Fatal error: could not open pipe");
  }
  for (auto signum : signalList) {
    if (::signal(signum, signalHandler) == SIG_ERR) {
      qFatal("Fatal error: could not install signal %d handler", signum);
    }
  }

  unixNotifier = new QSocketNotifier(unixFds[0], QSocketNotifier::Read, this);
  QObject::connect(unixNotifier, SIGNAL(activated(int)), this, SIGNAL(caughtFatalSignal()));
}
