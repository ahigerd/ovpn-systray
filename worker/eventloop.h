#ifndef OSW_EVENTLOOP_H
#define OSW_EVENTLOOP_H

#include <poll.h>
#include <vector>
#include "eventsource.h"

class EventLoop : private EventSource {
public:
  static EventLoop* instance();

  EventLoop();
  virtual ~EventLoop();

  bool poll();
  void run();
  void quit();

  void add(EventSource* source);
  void remove(EventSource* source);
  void dispatchSignal(int signum);

protected:
  void triggered();

private:
  std::vector<pollfd> pollFds;
  std::vector<EventSource*> sources;
  bool shouldQuit;
};

#endif
