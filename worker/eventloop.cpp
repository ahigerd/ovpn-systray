#include "eventloop.h"
#include "subprocess.h"
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <iostream>

static int eventWriteFd = 0;
static EventLoop* EventLoop_instance = nullptr;
static const std::vector<int> signalList{ SIGCHLD, SIGHUP, SIGTERM, SIGINT, SIGUSR1, SIGUSR2, SIGQUIT, SIGILL, SIGABRT, SIGBUS, SIGSEGV };

void signalHandler(int signum) {
  EventLoop::instance()->dispatchSignal(signum);
}

EventLoop* EventLoop::instance() {
  return EventLoop_instance;
}

EventLoop::EventLoop() : EventSource(), shouldQuit(false) {
  EventLoop_instance = this;
  int fds[2];
  ::pipe(fds);
  eventFd = fds[0];
  eventWriteFd = fds[1];
  add(this);

  for (auto signum : signalList) {
    if (::signal(signum, signalHandler) == SIG_ERR) {
      std::cerr << "Fatal error: could not install signal " << signum << " handler" << std::endl;
      std::exit(1);
    }
  }
}

EventLoop::~EventLoop() {
  EventLoop_instance = nullptr;
}

void EventLoop::dispatchSignal(int signum) {
  char buf[2] = { char(signum & 0xFF), '\0' };
  ::write(eventWriteFd, buf, 1);
}

void EventLoop::add(EventSource* source) {
  sources.push_back(source);
  pollFds.push_back((pollfd){
    source->eventFd,
    POLLIN | POLLPRI | POLLRDHUP | POLLHUP,
    0,
  });
  fcntl(source->eventFd, F_SETFL, O_NONBLOCK);
}

void EventLoop::remove(EventSource* source) {
  for (auto iter = sources.begin(); iter != sources.end(); iter++) {
    if (*iter == source) {
      for (auto i2 = pollFds.begin(); i2 != pollFds.end(); i2++) {
        if (i2->fd == source->eventFd) {
          pollFds.erase(i2);
          break;
        }
      }
      sources.erase(iter);
      break;
    }
  }
}

bool EventLoop::poll() {
  sigset_t sigmask;
  sigemptyset(&sigmask);
  sigaddset(&sigmask, SIGCHLD);
  for (pollfd& fd : pollFds) {
    fd.revents = 0;
  }

  int ready = ppoll(pollFds.data(), pollFds.size(), nullptr, &sigmask);
  if (ready <= 0) {
    return false;
  }
  for (pollfd& fd : pollFds) {
    if (fd.revents) {
      for (EventSource* source : sources) {
        if (source->eventFd == fd.fd) {
          source->triggered();
          break;
        }
      }
    }
  }
  return true;
}

void EventLoop::triggered() {
  char buf[2];
  while (!shouldQuit && ::read(eventFd, buf, 1) > 0) {
    if (buf[0] == SIGCHLD) {
      bool ok = Subprocess::reapChildren();
      if (!ok) {
        std::cerr << "Fatal error trying to reap children" << std::endl;
        shouldQuit = true;
      }
    } else {
      // Any other signal in signalList should be considered a request to exit
      shouldQuit = true;
    }
  }
}

void EventLoop::quit() {
  dispatchSignal(SIGTERM);
}

void EventLoop::run() {
  shouldQuit = false;
  while (!shouldQuit) {
    bool ok = poll();
    if (!ok) {
      std::cerr << "Polling error" << std::endl;
      return;
    }
  }
}
