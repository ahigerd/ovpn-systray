#include "messagereceiver.h"
#include "messagesender.h"

MessageReceiver::MessageReceiver() : sender(nullptr) {
  // initializers only
}

MessageReceiver::~MessageReceiver() {
  if (sender) {
    sender->removeReceiver(this);
  }
}
