#ifndef OSW_OVPNPROCESS_H
#define OSW_OVPNPROCESS_H

#include "subprocess.h"
#include "messagesender.h"
#include <string>
class ConfigFile;

class OvpnProcess : public Subprocess, public MessageSender {
public:
  OvpnProcess(ConfigFile* config);

protected:
  void processLine(const std::string& line);
  void triggered();

  bool autoDisconnect;
};

#endif
