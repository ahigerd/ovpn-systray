#include "resolvconf.h"
#include <fstream>
#include <iostream>

static std::string startBoundary = "#-- the following lines were added by ovpn-systray --#\n";
static std::string endBoundary   = "#-- the preceding lines were added by ovpn-systray --#\n";

ResolvConf::ResolvConf(const std::string& path) : path(path) {
  // initializers only
}

void ResolvConf::load() {
  std::ifstream f(path, std::ios::in);
  if (!f) {
    // File does not exist. Consider it empty.
    return;
  }
  f.seekg(0, f.end);
  int len = f.tellg();
  std::string buffer(len, '\0');
  f.seekg(0, f.beg);
  f.read(&buffer[0], len);
  f.close();

  size_t bound = buffer.find(startBoundary);
  if (bound == std::string::npos) {
    if (buffer.find("nameserver") == 0) {
      bound = 0;
    } else {
      bound = buffer.find("\nnameserver");
    }
    if (bound == std::string::npos) {
      preamble = buffer.substr(0);
      postamble.clear();
    } else if (bound == 0) {
      preamble.clear();
      postamble = buffer.substr(0);
    } else {
      preamble = buffer.substr(0, bound + 1);
      postamble = buffer.substr(bound + 1);
    }
  } else {
    preamble = buffer.substr(0, bound);
    size_t after = buffer.find(endBoundary);
    if (after == std::string::npos) {
      // Appears to be corrupt, play it safe
      postamble = buffer.substr(bound + startBoundary.length());
    } else {
      postamble = buffer.substr(after + endBoundary.length());
    }
  }
}

void ResolvConf::save() {
  load();
  std::ofstream f(path, std::ios::out | std::ios::trunc);
  f << preamble << startBoundary;

  if (searchDomains.size() > 0) {
    f << "search";
    for (auto domain : searchDomains) {
      f << " " << domain;
    }
    f << "\n";
  }

  for (auto server : nameservers) {
    f << "nameserver " << server << "\n";
  }

  f << endBoundary << postamble;
  f.close();
}

void ResolvConf::revert() {
  {
    // Check to see if the file exists.
    // Leave it alone if it does not.
    std::ifstream f(path, std::ios::in);
    if (!f) {
      return;
    }
  }
  load();
  std::ofstream f(path, std::ios::out | std::ios::trunc);
  f << preamble << postamble;
  f.close();
}

void ResolvConf::addNameserver(const std::string& server) {
  nameservers.push_back(server);
}

void ResolvConf::addSearchDomain(const std::string& domain) {
  searchDomains.push_back(domain);
}
