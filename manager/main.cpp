#include <unistd.h>
#include <signal.h>
#include <QCoreApplication>
#include <QSettings>
#include <QSocketNotifier>
#include "../common/signalhandler.h"
#include "managementsocket.h"
#include "iconprocess.h"

int main(int argc, char** argv) {
  QCoreApplication::setApplicationName("ovpn-systray");
  QCoreApplication::setOrganizationName("Alkahest");
  QCoreApplication::setApplicationVersion("0.0.1");
  QCoreApplication app(argc, argv);

  QSettings settings;
  IconProcess icon;
  ManagementSocket ms;

  QObject::connect(&ms, SIGNAL(connectionStateChanged(int)), &icon, SLOT(updateStatus(int)));
  QObject::connect(&ms, SIGNAL(showMessage(QString, QString, int, int)),
      &icon, SLOT(showMessage(QString, QString, int, int)));
  QObject::connect(&ms, SIGNAL(needsAuth(QString, QString)), &icon, SLOT(getAuth(QString, QString)));
  QObject::connect(&ms, SIGNAL(needsAuthWithChallenge(QString, QString, bool, QString)),
      &icon, SLOT(getAuthWithChallenge(QString, QString, bool, QString)));
  QObject::connect(&ms, SIGNAL(needsWebAuth(QString)), &icon, SLOT(getWebAuth(QString)));

  QObject::connect(&icon, SIGNAL(authenticate(QString, QString, QString)),
      &ms, SLOT(authenticate(QString, QString, QString)));
  QObject::connect(&icon, SIGNAL(authenticate(QString, QString, QString, QString)),
      &ms, SLOT(authenticate(QString, QString, QString, QString)));
  QObject::connect(&icon, SIGNAL(authenticate(QString, QString, QByteArray, QString)),
      &ms, SLOT(authenticate(QString, QString, QByteArray, QString)));
  QObject::connect(&icon, SIGNAL(connectToVpn()), &ms, SLOT(connectToVpn()));
  QObject::connect(&icon, SIGNAL(disconnectFromVpn()), &ms, SLOT(disconnectFromVpn()));
  QObject::connect(&icon, SIGNAL(shutdown()), &app, SLOT(quit()));

  if (!icon.launchIcon()) {
    qFatal("Fatal error: unable to launch ovpn-systray-icon");
  }

  if (settings.value("autoConnect").toBool()) {
    ms.connectToVpn();
  }

  int rv = app.exec();
  ms.disconnectFromVpn();
  return rv;
}
