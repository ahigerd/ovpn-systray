TEMPLATE = app
DESTDIR = ..
TARGET = ovpn-systray-worker
OBJECTS_DIR = .tmp
CONFIG -= qt
CONFIG += debug
INCLUDEPATH += .

HEADERS += messagesender.h   messagereceiver.h   eventsource.h   eventloop.h
SOURCES += messagesender.cpp messagereceiver.cpp eventsource.cpp eventloop.cpp

HEADERS += linebuffer.h   subprocess.h   ovpnprocess.h   managementsocket.h   stdin.h
SOURCES += linebuffer.cpp subprocess.cpp ovpnprocess.cpp managementsocket.cpp stdin.cpp

HEADERS += updownhandler.h   passwordhandler.h   resolvconf.h   configfile.h
SOURCES += updownhandler.cpp passwordhandler.cpp resolvconf.cpp configfile.cpp

SOURCES += main.cpp
