#ifndef OSW_UPDOWNHANDLER_H
#define OSW_UPDOWNHANDLER_H

#include "messagereceiver.h"
#include <map>
#include <string>

class UpDownHandler : public MessageReceiver {
public:
  UpDownHandler(bool useResolvectl, const std::string& resolvConfPath);

protected:
  void dispatch(const std::string& message);

private:
  void updateNetwork();
  void addRoute(const std::string& network, const std::string& netmask, const std::string& gateway);
  void upDevice(const std::string& name, const std::string& address, const std::string& peer, const std::string& mtu);

  std::map<std::string, std::string> env;
  bool useResolvectl;
  std::string resolvConfPath;
};

#endif
