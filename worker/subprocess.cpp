#include "subprocess.h"
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include <cstring>
#include <iostream>

static std::vector<Subprocess*> subprocesses = std::vector<Subprocess*>();

bool Subprocess::reapChildren() {
  while (true) {
    int status = 0;
    int pid = ::waitpid(-1, &status, WNOHANG);
    if (pid <= 0) {
      return pid == 0 || errno == ECHILD;
    }
    for (Subprocess* p : subprocesses) {
      if (p->pid == pid) {
        p->pid = 0;
        p->status = WEXITSTATUS(status);
        p->triggered();
      }
    }
  }
}

Subprocess::Subprocess(const std::vector<std::string>& argv) : LineBuffer(), pid(0), status(0) {
  subprocesses.push_back(this);

  std::string cmdline;
  std::vector<const char*> c_argv;
  for (const std::string& arg : argv) {
    cmdline += arg + " ";
    c_argv.push_back(arg.c_str());
  }
  c_argv.push_back(nullptr);

  run(cmdline, c_argv);
}

Subprocess::Subprocess(std::initializer_list<std::string> argv) : LineBuffer(), pid(0), status(0) {
  subprocesses.push_back(this);

  std::string cmdline;
  std::vector<const char*> c_argv;
  for (const std::string& arg : argv) {
    cmdline += arg + " ";
    c_argv.push_back(arg.c_str());
  }
  c_argv.push_back(nullptr);

  run(cmdline, c_argv);
}

void Subprocess::run(const std::string& cmdline, const std::vector<const char*> c_argv) {
  std::cerr << "[SUB] " << cmdline << std::endl;
  int pipefd[2];
  ::pipe(pipefd);
  pid = ::fork();
  if (pid == 0) {
    // Child process
    ::close(pipefd[0]);
    ::dup2(pipefd[1], STDIN_FILENO);
    ::dup2(pipefd[1], STDOUT_FILENO);
    ::dup2(pipefd[1], STDERR_FILENO);
    ::execvp(c_argv[0], const_cast<char* const*>(c_argv.data()));
    // If execvp is successful, this will never be executed
    std::cerr << "error" << errno << ": " << std::strerror(errno) << std::endl;
    std::exit(1);
  } else {
    ::close(pipefd[1]);
    setFd(pipefd[0]);
  }
}

Subprocess::~Subprocess() {
  stopTracking();
  if (pid) {
    kill();
  }
}

void Subprocess::stopTracking() {
  for (auto iter = subprocesses.begin(); iter != subprocesses.end(); iter++) {
    if (*iter == this) {
      subprocesses.erase(iter);
      break;
    }
  }
}

bool Subprocess::isRunning() const {
  return !!pid;
}

bool Subprocess::kill() {
  if (pid) {
    ::kill(pid, SIGKILL);
    int status;
    waitpid(pid, &status, 0);
    pid = 0;
    return true;
  }
  return false;
}

void Subprocess::write(const std::string& data) {
  ::write(eventFd, data.c_str(), data.length());
}

int Subprocess::waitForFinished() {
  stopTracking();
  int stat_val = 0;
  ::waitpid(pid, &stat_val, 0);
  status = WEXITSTATUS(stat_val);
  pid = 0;
  return status;
}

int Subprocess::exitStatus() const {
  return status;
}
