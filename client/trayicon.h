#ifndef OSC_TRAYICON_H
#define OSC_TRAYICON_H

#include <QSystemTrayIcon>
#include <QIcon>
class QAction;

class TrayIcon : public QObject {
Q_OBJECT
public:
  TrayIcon(QObject* parent = nullptr);
  ~TrayIcon();

signals:
  void shouldConnect();
  void shouldDisconnect();
  void preferencesClicked();
  void shutdown();

public slots:
  void updateStatus(int state);
  void showMessage(const QString& title, const QString& message, QSystemTrayIcon::MessageIcon icon = QSystemTrayIcon::Information, int timeout = 10000);

private slots:
  void showMenu(QSystemTrayIcon::ActivationReason reason);
  void chooseIcon(const QIcon& icon);
  void updateVisibility();

private:
  QIcon iconOK, iconOffline, iconConnecting, iconError, currentIcon;
  QMenu* menu;
  QAction *aConnect, *aDisconnect;
  QSystemTrayIcon* trayIcon;
};

#endif
