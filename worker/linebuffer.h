#ifndef OSW_LINEBUFFER_H
#define OSW_LINEBUFFER_H

#include "eventsource.h"
#include <string>

class LineBuffer : public EventSource {
public:
  LineBuffer();
  virtual ~LineBuffer();

  void setFd(int fd);
  void close();
  bool fillBuffer();
  bool canReadLine() const;
  std::string readLine();

protected:
  void triggered();
  virtual void processLine(const std::string& line);
  std::string buffer;
};

#endif
