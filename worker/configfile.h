#ifndef OSW_CONFIGFILE_H
#define OSW_CONFIGFILE_H

#include <string>

class ConfigFile {
public:
  ConfigFile(const std::string& path);

  std::string ovpnPath, configPath, socketPath, resolvConfPath;
  bool useResolvectl;
  bool autoDisconnect;
};

#endif

