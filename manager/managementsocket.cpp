#include "managementsocket.h"
#include "unistd.h"
#include <QCoreApplication>
#include <QSettings>
#include <QTimer>
#include <QProcess>
#include <QtDebug>

ManagementSocket::ManagementSocket(QObject* parent)
  : QObject(parent), shouldConnect(false), isConnected(false)
{
  respawnTimer = new QTimer(this);
  respawnTimer->setSingleShot(true);
  respawnTimer->setInterval(30000);
  QObject::connect(respawnTimer, SIGNAL(timeout()), this, SLOT(reconnectToVpn()));

  process = new QProcess(this);
  process->setProcessChannelMode(QProcess::ForwardedErrorChannel);
  QObject::connect(process, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(daemonTerminated()));
  QObject::connect(process, SIGNAL(readyReadStandardOutput()), this, SLOT(dataReceived()));
}

void ManagementSocket::dataReceived() {
  buffer += QString::fromUtf8(process->readAllStandardOutput());
  int pos;
  while ((pos = buffer.indexOf('\n')) >= 0) {
    QString line = buffer.left(pos).trimmed();
    buffer.remove(0, pos + 1);
    processLine(line);
  }
}

void ManagementSocket::processLine(const QString& line) {
  qDebug() << "--" << line;
  if (line[0] == '>') {
    QString key = line.section(':',0, 0).remove(0, 1);
    QString data = line.section(':', 1);
    if (key == "PASSWORD") {
      handlePassword(data);
    } else if (key == "FATAL") {
      handleFatal(data);
    } else if (key == "UPDOWN") {
      handleUpDown(data);
    } else if (key == "INFOMSG") {
      handleInfoMsg(data);
    }
  }
}

void ManagementSocket::reconnectToVpn() {
  if (shouldConnect) {
    connectToVpn();
  }
}

void ManagementSocket::connectToVpn() {
  shouldConnect = true;
  failMessage = "Unable to connect to VPN";
  process->start(QCoreApplication::instance()->applicationDirPath() + "/ovpn-systray-worker", QStringList() << QSettings().fileName());
  emit connectionStateChanged(1);
}

void ManagementSocket::disconnectFromVpn() {
  shouldConnect = false;
  respawnTimer->stop();
  if (process->state() == QProcess::Running) {
    process->write("signal SIGTERM\nquit\n");
  }
  if (process->state() != QProcess::NotRunning) {
    bool stopped = process->waitForFinished(1000);
    if (!stopped) {
      process->terminate();
      stopped = process->waitForFinished(1000);
    }
    if (!stopped) {
      process->kill();
    }
  }
  emit connectionStateChanged(0);
}

void ManagementSocket::handlePassword(const QString& data) {
  // >PASSWORD:Verification Failed: 'Auth'
  if (data.startsWith("Verification Failed")) {
    failMessage = data;
    return;
  }

  // >PASSWORD:Auth-Token:token
  if (data.section(':', 0, 0) == "Auth-Token") {
    // This is the server telling us to use this token in the future.
    // We don't currently support using it for reauthorization.
    return;
  }

  // >PASSWORD:Need 'SCOPE' username/password SC:ECHO,CHALLENGE
  QString challenge = data.section("SC:", 1);
  QString message = data.section("SC:", 0, 0);
  QString scope = message.section("'", 1, 1);

  if (challenge.isEmpty()) {
    // No static challenge
    emit needsAuth(scope, message);
  } else {
    // static challenge
    bool shouldEcho = challenge.section(',', 0, 0) == "1";
    QString prompt = challenge.section(',', 1);
    emit needsAuthWithChallenge(scope, message, shouldEcho, prompt);
  }
}

void ManagementSocket::handleFatal(const QString& data) {
  failMessage = data;
  disconnectFromVpn();
}

void ManagementSocket::handleUpDown(const QString& data) {
  if (data == "CONNECTING") {
    failMessage.clear();
    emit connectionStateChanged(1);
  } else if (data == "DOWN") {
    emit connectionStateChanged(3);
  } else if (data == "UP") {
    emit connectionStateChanged(2);
    emit showMessage("ovpn-systray", "Connected to VPN", 1 /*QSystemTrayIcon::Information*/);
  }
}

void ManagementSocket::handleInfoMsg(const QString& data) {
  qDebug() << data;
  if (data.startsWith("WEB_AUTH::")) {
    emit needsWebAuth(data.section("::", 1));
  }
}

void ManagementSocket::authenticate(const QString& scope, const QString& username, const QString& password) {
  process->write(QString("username \"%1\" \"%2\"\npassword \"%1\" \"%3\"\n").arg(scope).arg(username).arg(password).toUtf8());
}

void ManagementSocket::authenticate(const QString& scope, const QString& username, const QString& password, const QString& challenge) {
  authenticate(scope, username, password.toUtf8().toBase64(), challenge);
}

void ManagementSocket::authenticate(const QString& scope, const QString& username, const QByteArray& password, const QString& challenge) {
  QByteArray response = "SCRV1:" + password + ":" + challenge.toUtf8().toBase64();
  authenticate(scope, username, QString::fromUtf8(response));
}

void ManagementSocket::daemonTerminated() {
  qDebug() << "daemon terminated";
  if (failMessage.isEmpty()) {
    failMessage = "Disconnected from VPN";
  }
  emit showMessage("ovpn-systray", failMessage, shouldConnect ? 3 /*QSystemTrayIcon::Critical*/ : 1 /*QSystemTrayIcon::Information*/);
  isConnected = false;
  emit connectionStateChanged(0);
  if (shouldConnect) {
    respawnTimer->start();
  }
}
