#ifndef OSM_MANAGEMENTSOCKET_H
#define OSM_MANAGEMENTSOCKET_H

#include <QObject>
class QProcess;
class QTimer;

class ManagementSocket : public QObject {
Q_OBJECT
public:
  ManagementSocket(QObject* parent = nullptr);

signals:
  void showMessage(const QString& title, const QString& message, int icon = 1, int timeout = 10000);
  void connectionStateChanged(int state);
  void needsAuth(const QString& scope, const QString& message);
  void needsAuthWithChallenge(const QString& scope, const QString& message, bool echo, const QString& challenge);
  void needsWebAuth(const QString& url);

public slots:
  void connectToVpn();
  void disconnectFromVpn();
  void authenticate(const QString& scope, const QString& username, const QString& password);
  void authenticate(const QString& scope, const QString& username, const QString& password, const QString& challenge);
  void authenticate(const QString& scope, const QString& username, const QByteArray& password, const QString& challenge);

private slots:
  void reconnectToVpn();
  void dataReceived();
  void processLine(const QString& line);
  void daemonTerminated();

private:
  QProcess* process;
  QTimer* respawnTimer;
  QString buffer;
  QString failMessage;
  bool shouldConnect;
  bool isConnected;

  void handlePassword(const QString& data);
  void handleFatal(const QString& data);
  void handleUpDown(const QString& data);
  void handleInfoMsg(const QString& data);
};

#endif
