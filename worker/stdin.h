#ifndef OSW_STDIN_H
#define OSW_STDIN_H

#include "linebuffer.h"
#include "messagesender.h"

class Stdin : public LineBuffer, public MessageSender {
public:
  Stdin();

protected:
  void processLine(const std::string& line);
};

#endif
