#ifndef OSW_PASSWORDHANDLER_H
#define OSW_PASSWORDHANDLER_H

#include "messagereceiver.h"
#include <map>
#include <string>

class PasswordHandler : public MessageReceiver {
public:
  PasswordHandler();

protected:
  void dispatch(const std::string& message); 
};

#endif

