#include "updownhandler.h"
#include "subprocess.h"
#include "resolvconf.h"
#include <iostream>
#include <sstream>
#include <vector>
#include <arpa/inet.h>
#include <cstdlib>
#include <unistd.h>

static int getNumBits(unsigned char* bytes, int len) {
  int result = 0;
  while (--len >= 0) {
    while (bytes[len]) {
      if (bytes[len] & 0x01) {
        result++;
      }
      bytes[len] >>= 1;
    }
  }
  return result;
}

static int maskToLength(const std::string& mask) {
  if (mask.find(':') == std::string::npos) {
    // it's an IPv4 address
    in_addr dest;
    int ok = ::inet_pton(AF_INET, mask.c_str(), &dest) == 1;
    if (!ok) return 0;
    return getNumBits(reinterpret_cast<unsigned char*>(&dest.s_addr), sizeof(dest.s_addr));
  } else {
    // it's an IPv6 address
    in6_addr dest;
    int ok = ::inet_pton(AF_INET6, mask.c_str(), &dest) == 1;
    if (!ok) return 0;
    return getNumBits(dest.s6_addr, sizeof(dest.s6_addr));
  }
}

UpDownHandler::UpDownHandler(bool useResolvectl, const std::string& resolvConfPath)
: MessageReceiver(), useResolvectl(useResolvectl), resolvConfPath(resolvConfPath) {
  // initializers only
}

void UpDownHandler::dispatch(const std::string& message) {
  if (message == "CONNECTING") {
    std::cout << ">UPDOWN:CONNECTING" << std::endl;
  } else if (message == "UP") {
    env.clear();
    std::cout << ">UPDOWN:CONNECTING" << std::endl;
  } else if (message == "DOWN") {
    env.clear();
    std::cout << ">UPDOWN:DOWN" << std::endl;
    if (useResolvectl) {
      Subprocess revert({ "resolvectl", "revert", env["dev"] });
    } else {
      ResolvConf("/etc/resolv.conf.head").revert();
      ResolvConf("/etc/resolv.conf.tail").revert();
      ResolvConf().revert();
    }
    // TODO: is it necessary to tear down the device?
  } else if (message == "ENV,END") {
    // Sleep for a bit before updating the network settings, to make
    // sure OpenVPN has had a chance to get the device created and
    // the OS has had a chance to process ifup scripts.
    // TODO: this works for me, but it might be better served as a
    // configuration option
    ::sleep(1);
    updateNetwork();
  } else if (message.compare(0, 4, "ENV,") == 0) {
    size_t comma = message.find('=');
    std::string key = message.substr(4, comma - 4);
    std::string value = message.substr(comma + 1);
    env[key] = value;
  }
}

void UpDownHandler::updateNetwork() {
  // TODO: add an option to periodically make sure the VPN is still configured
#ifndef NDEBUG
  for (auto pair : env) {
    std::cout << "* " << pair.first << " = " << pair.second << std::endl;
  }
#endif

  upDevice(env["dev"], env["ifconfig_local"], env["ifconfig_remote"], env["tun_mtu"]);

  int i = 1;
  std::string index;
  std::stringstream ss;
  while (true) {
    ss << i;
    index = ss.str();
    ss.str(std::string());
    if (!env.count("route_network_" + index)) {
      break;
    }
    addRoute(
      env["route_network_" + index],
      env["route_netmask_" + index],
      env["route_gateway_" + index]
    );
    ++i;
  }

  // Make sure the loopback interface stays configured
  // XXX: Is this an OpenVPN bug? A NetworkManager bug? Should this be a configuration option?
  upDevice("lo", "127.0.0.1", std::string(), std::string());

  std::vector<std::string> rcDns({ "resolvectl", "dns", env["dev"]});
  std::vector<std::string> rcDomain({ "resolvectl", "domain", env["dev"]});
  ResolvConf resolv(resolvConfPath);
  i = 1;
  while (true) {
    ss << i;
    index = ss.str();
    ss.str(std::string());
    if (!env.count("foreign_option_" + index)) {
      break;
    }
    std::string value = env["foreign_option_" + index];
    if (value.compare(0, 16, "dhcp-option DNS ") == 0) {
      rcDns.push_back(value.substr(16));
      resolv.addNameserver(value.substr(16));
    } else if (value.compare(0, 26, "dhcp-option DOMAIN-SEARCH ") == 0) {
      rcDomain.push_back(value.substr(26));
      resolv.addSearchDomain(value.substr(26));
    }
    ++i;
  }
  if (useResolvectl) {
    Subprocess dnsProcess(rcDns);
    int result = dnsProcess.waitForFinished();
    std::cerr << "[LOG] resolvectl dns returned " << result << std::endl;
    Subprocess domainProcess(rcDomain);
    result = domainProcess.waitForFinished();
    std::cerr << "[LOG] resolvectl domain returned " << result << std::endl;
  } else {
    resolv.save();
  }
  std::cout << ">UPDOWN:UP" << std::endl;
}

void UpDownHandler::addRoute(const std::string& network, const std::string& netmask, const std::string& gateway) {
  std::stringstream ss;
  ss << network << "/" << maskToLength(netmask);
  Subprocess process({
    "/sbin/ip", "route",
    "add", ss.str(),
    "via", gateway
  });
  int result = process.waitForFinished();
  std::cerr << "[LOG] ip route returned " << result << std::endl;
}

void UpDownHandler::upDevice(const std::string& name, const std::string& address, const std::string& peer, const std::string& mtu) {
  int result;
  if (mtu.empty()) {
    Subprocess process({
      "/sbin/ip", "link",
      "set", "dev", name,
      "up",
    });
    result = process.waitForFinished();
  } else {
    Subprocess process({
      "/sbin/ip", "link",
      "set", "dev", name,
      "up",
      "mtu", mtu
    });
    result = process.waitForFinished();
  }
  std::cerr << "[LOG] ip link returned " << result << std::endl;

  if (peer.empty()) {
    Subprocess process2({
      "/sbin/ip", "addr", "add",
      address,
      "dev", name,
    });
    result = process2.waitForFinished();
  } else {
    Subprocess process2({
      "/sbin/ip", "addr", "add",
      "dev", name,
      "local", address,
      "peer", peer
    });
    result = process2.waitForFinished();
  }
  std::cerr << "[LOG] ip addr returned " << result << std::endl;
}
