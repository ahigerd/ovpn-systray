#ifndef OSW_MESSAGESENDER_H
#define OSW_MESSAGESENDER_H

#include <string>
#include <map>
class MessageReceiver;

class MessageSender {
public:
  bool send(const std::string& key, const std::string& message);

  void addReceiver(const std::string& key, MessageReceiver* receiver);
  void removeReceiver(MessageReceiver* receiver);

private:
  std::map<std::string, MessageReceiver*> rcv;
};

#endif
