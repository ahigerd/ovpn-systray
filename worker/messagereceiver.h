#ifndef OSW_MESSAGERECEIVER_H
#define OSW_MESSAGERECEIVER_H

#include <string>
class MessageSender;

class MessageReceiver {
public:
  MessageReceiver();
  virtual ~MessageReceiver();

protected:
  friend class MessageSender;
  virtual void dispatch(const std::string& message) = 0; 
  MessageSender* sender;
};

#endif
