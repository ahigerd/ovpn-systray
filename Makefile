all: ovpn-systray ovpn-systray-icon ovpn-systray-worker

clean:
	$(MAKE) -C manager clean
	$(MAKE) -C client clean
	$(MAKE) -C worker clean

distclean:
	$(MAKE) -C manager distclean
	$(MAKE) -C client distclean
	$(MAKE) -C worker distclean

install: all
	install -m 775 ovpn-systray /usr/bin/ovpn-systray
	install -m 775 ovpn-systray-icon /usr/bin/ovpn-systray-icon
	install -m 4775 -o root ovpn-systray-worker /usr/bin/ovpn-systray-worker

local: all
	chown root ovpn-systray-worker
	chmod 4775 ovpn-systray-worker

client/Makefile: client/client.pro
	qmake -o $@ $<

manager/Makefile: manager/manager.pro
	qmake -o $@ $<

worker/Makefile: worker/worker.pro
	qmake -o $@ $<

ovpn-systray: manager/Makefile FORCE
	$(MAKE) -C manager

ovpn-systray-icon: client/Makefile FORCE
	$(MAKE) -C client

ovpn-systray-worker: worker/Makefile FORCE
	$(MAKE) -C worker

FORCE:
