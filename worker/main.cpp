#include "eventloop.h"
#include "ovpnprocess.h"
#include "managementsocket.h"
#include "updownhandler.h"
#include "passwordhandler.h"
#include "stdin.h"
#include "resolvconf.h"
#include "configfile.h"
#include <string>
#include <iostream>
#include <sys/stat.h>
#include <unistd.h>

int main(int argc, char** argv) {
  if (argc != 2) {
    std::cerr << "Usage: ovpn-systray-worker CONFIG_FILE" << std::endl;
    return 1;
  }

  struct stat sb;
  if (::stat(argv[1], &sb) != 0) {
    std::cerr << "Config file '" << argv[1] << "' could not be read" << std::endl;
    return 1;
  }

  ConfigFile config(argv[1]);

  if (config.ovpnPath.empty() || config.socketPath.empty() || config.configPath.empty()) {
    std::cerr << "Config file '" << argv[1] << "' does not contain a valid configuration" << std::endl;
    return 1;
  }

  EventLoop app;

  // Escalate privileges to access root capabilities.
  // This will fail if the binary is not setuid.
  // TODO: shed all capabilities not necessary for netlink.
  ::setuid(0);

  ManagementSocket ms(config.socketPath);
  if (!ms.listen()) {
    return 1;
  }

  OvpnProcess ovpn(&config);
  if (!ovpn.isRunning()) {
    std::cerr << "failed to launch OpenVPN" << std::endl;
    return 1;
  }

  UpDownHandler upDown(config.useResolvectl, config.resolvConfPath);
  std::cerr << (config.useResolvectl ? std::string("resolvectl") : config.resolvConfPath) << std::endl;
  ms.addReceiver("UPDOWN", &upDown);
  ovpn.addReceiver("UPDOWN", &upDown);

  PasswordHandler pw;
  ms.addReceiver("PASSWORD", &pw);

  Stdin in;
  in.addReceiver("STDIN", &ms);

  app.run();

  // TODO: cleanup
  if (!config.useResolvectl) {
    ResolvConf("/etc/resolv.conf.head").revert();
    ResolvConf("/etc/resolv.conf.tail").revert();
    ResolvConf().revert();
  }

  return 0;
}
