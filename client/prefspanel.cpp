#include "prefspanel.h"
#include <QSettings>
#include <QGridLayout>
#include <QDialogButtonBox>
#include <QLabel>
#include <QLineEdit>
#include <QFileDialog>
#include <QGroupBox>
#include <QFile>
#include <QMessageBox>
#include <QToolButton>
#include <QCheckBox>
#include <QComboBox>
#include <QDir>

PrefsPanel::PrefsPanel(QWidget* parent) : QDialog(parent) {
  setWindowTitle("ovpn-systray Preferences");
  setWindowIcon(QIcon(":/icons/network-vpn-symbolic.symbolic.png"));

  QSettings settings;
  QGridLayout* l = new QGridLayout(this);

  QLabel* lConfigPath = new QLabel("&Configuration File:", this);
  eConfigPath = new QLineEdit(settings.value("config").toString(), this);
  QToolButton* bConfigPath = new QToolButton(this);
  bConfigPath->setText("...");
  lConfigPath->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
  lConfigPath->setBuddy(eConfigPath);
  l->addWidget(lConfigPath, 0, 0);
  l->addWidget(eConfigPath, 0, 1);
  l->addWidget(bConfigPath, 0, 2);

  QLabel* lUsername = new QLabel("&Username:", this);
  eUsername = new QLineEdit(settings.value("username").toString(), this);
  lUsername->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
  lUsername->setBuddy(eUsername);
  l->addWidget(lUsername, 1, 0);
  l->addWidget(eUsername, 1, 1, 1, 2);

  QLabel* lPassword = new QLabel("&Password:", this);
  ePassword = new QLineEdit(this);
  ePassword->setEchoMode(QLineEdit::Password);
  if (settings.contains("password")) {
    ePassword->setText("********");
  }
  lPassword->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
  lPassword->setBuddy(ePassword);
  l->addWidget(lPassword, 2, 0);
  l->addWidget(ePassword, 2, 1, 1, 2);

  cAutoConnect = new QCheckBox("Automatically &connect on startup", this);
  cAutoConnect->setChecked(settings.value("autoConnect").toBool());
  l->addWidget(cAutoConnect, 3, 1, 1, 2);

  cAutoDisconnect = new QCheckBox("Automatically &disconnect when network disconnected", this);
  cAutoDisconnect->setChecked(settings.value("autoDisconnect").toBool());
  l->addWidget(cAutoDisconnect, 4, 1, 1, 2);

  cAutoWebAuth = new QCheckBox("Automatically open &web browser for WEB_AUTH", this);
  cAutoWebAuth->setChecked(settings.value("autoWebAuth").toBool());
  l->addWidget(cAutoWebAuth, 5, 1, 1, 2);

  QGroupBox* gb = new QGroupBox("Advanced", this);
  QGridLayout* l2 = new QGridLayout(gb);
  l->addWidget(gb, 6, 0, 1, 3);

  QLabel* lBinPath = new QLabel("Open&VPN Path:", gb);
  eBinPath = new QLineEdit(settings.value("openvpnPath", "/usr/sbin/openvpn").toString(), gb);
  lBinPath->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
  lBinPath->setBuddy(eBinPath);
  l2->addWidget(lBinPath, 0, 0);
  l2->addWidget(eBinPath, 0, 1);

  QLabel* lSocketPath = new QLabel("Control &Socket Path:", gb);
  eSocketPath = new QLineEdit(settings.value("socketPath", "/tmp/ovpn-systray").toString(), gb);
  lSocketPath->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
  lSocketPath->setBuddy(eSocketPath);
  l2->addWidget(lSocketPath, 1, 0);
  l2->addWidget(eSocketPath, 1, 1);

  QLabel* lResolvConf = new QLabel("Update &DNS settings using:", this);
  pResolvConf = new QComboBox(this);
  pResolvConf->addItem("resolvectl");
  pResolvConf->addItem("/etc/resolv.conf");
  pResolvConf->addItem("/etc/resolv.conf.head");
  pResolvConf->addItem("/etc/resolv.conf.tail");
  if (settings.value("useResolvectl").toBool()) {
    pResolvConf->setCurrentIndex(0);
  } else if (settings.value("useResolvConfHead").toBool()) {
    pResolvConf->setCurrentIndex(2);
  } else if (settings.value("useResolvConfTail").toBool()) {
    pResolvConf->setCurrentIndex(3);
  } else if (settings.contains("useResolvectl")) {
    pResolvConf->setCurrentIndex(1);
  } else {
    pResolvConf->setCurrentIndex(0);
  }
  lResolvConf->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
  lResolvConf->setBuddy(pResolvConf);
  l2->addWidget(lResolvConf, 2, 0);
  l2->addWidget(pResolvConf, 2, 1);

  QDialogButtonBox* bb = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal, this);
  l->addWidget(bb, 7, 0, 1, 3);

  QObject::connect(bb, SIGNAL(accepted()), this, SLOT(save()));
  QObject::connect(bb, SIGNAL(rejected()), this, SLOT(reject()));
  QObject::connect(bConfigPath, SIGNAL(clicked()), this, SLOT(browseConfigFile()));
}

void PrefsPanel::save() {
  QSettings settings;

  // TODO: it would be better to have these warnings presented in a better way, but this is enough to get it up and running.
  if (!QFile::exists(eConfigPath->text())) {
    QMessageBox::warning(this, "ovpn-systray", QString("The requested configuration file '%1' could not be found.").arg(eConfigPath->text()));
    return;
  }
  if (!QFile::exists(eBinPath->text())) {
    QMessageBox::warning(this, "ovpn-systray", QString("The requested OpenVPN binary '%1' could not be found.").arg(eBinPath->text()));
    return;
  }

  settings.setValue("username", eUsername->text());
  if (ePassword->text().isEmpty()) {
    settings.remove("password");
  } else if (ePassword->text() != "********") {
    settings.setValue("password", QString::fromUtf8(ePassword->text().toUtf8().toBase64()));
  }

  if (cAutoDisconnect->isChecked() != settings.value("autoDisconnect").toBool()) {
    QMessageBox::information(this, "ovpn-systray", "Changes to automatic disconnect will take effect on restart.");
  }

  settings.setValue("config", eConfigPath->text());
  settings.setValue("openvpnPath", eBinPath->text());
  settings.setValue("socketPath", eSocketPath->text());
  settings.setValue("autoConnect", cAutoConnect->isChecked());
  settings.setValue("autoDisconnect", cAutoDisconnect->isChecked());
  settings.setValue("autoWebAuth", cAutoWebAuth->isChecked());

  int resolvMode = pResolvConf->currentIndex();
  settings.setValue("useResolvectl", resolvMode == 0);
  if (resolvMode == 0) {
    settings.remove("useResolvConfHead");
    settings.remove("useResolvConfTail");
  } else {
    settings.setValue("useResolvConfHead", resolvMode == 2);
    settings.setValue("useResolvConfTail", resolvMode == 3);
  }

  settings.sync();
  accept();
}

void PrefsPanel::browseConfigFile() {
  QString initialPath = eConfigPath->text();
  if (initialPath.isEmpty()) {
    initialPath = QDir::homePath();
  }
  QString path = QFileDialog::getOpenFileName(this, "Select OpenVPN Configuration File", initialPath);
  if (path.isEmpty()) {
    return;
  }
  eConfigPath->setText(path);
}
